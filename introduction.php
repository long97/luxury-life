<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Luxury Life | Về chúng tôi</title>
    <link rel="shortcut icon" href="img/logo.jpg">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <style type="text/css">
      .noi-dung{
        text-align: justify;
      }
    </style>
  </head>

  <body>

    <!-- Navigation -->
    <?php include('navigation.php');?>
    <?php include('slide.php');?>
   

<!-- Page Content -->
    <div class="container" style="margin-top: 100px;">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3 my-4" style="margin-top: 100px">VỀ CHÚNG TÔI</h1>

      <ol class="breadcrumb" style="margin-top: 80px; background-color: white">
        <li class="breadcrumb-item">
          <a href="index.html" style="color: black">TRANG CHỦ</a>
        </li>
        <li class="breadcrumb-item active">VỀ CHÚNG TÔI</li>
      </ol>

      <!-- Content Row -->
      <div class="row">
        <!-- Sidebar Column -->
        <div class="col-lg-3 mb-4 ">

          <div class="list-group" style="margin-top: 50px;">
            <a href="#" class="list-group-item "><img src="img/introduction/Ong_Nguyen_Hoang_long.jpg" style="width: 215px;"></a>
            <h5 style="text-align: center;">Ông Nguyễn Hoàng Long</h5>
            <p style="text-align: center;">Giám đốc điều hành</p>
          </div>
          <div class="list-group ">
            <a href="#" class="list-group-item "><img src="img/introduction/Ba_Hoang_Thi_Hong_Hanh.jpg" style="width: 215px;"></a>
            <h5 style="text-align: center;">Bà Hoàng Thị Hồng Hạnh</h5>
            <p style="text-align: center;">Giám đốc tài chính</p>
          </div>
          <div class="list-group ">
            <a href="#" class="list-group-item "><img src="img/introduction/Ba_Nguyen_Thi_Thuy_Linh.jpg" style="width: 215px;"></a>
            <h5 style="text-align: center;">Bà Nguyễn Thị Thùy Linh</h5>
            <p style="text-align: center;">Giám đốc nhân sự</p>
          </div>
          <div class="list-group ">
            <a href="#" class="list-group-item "><img src="img/introduction/Ong_Doan_Xuan_Bach.jpg" style="width: 215px;"></a>
            <h5 style="text-align: center;">Ông Đoàn Xuân Bách</h5>
            <p style="text-align: center;">Giám đốc kinh doanh</p>
          </div>
        </div>
        <!-- Content Column -->
        <div class="col-lg-8 noi-dung" style="margin-top: 30px">
          <h1 style="text-align: center; margin-bottom: 50px;">Giới thiệu về Luxury Life</h1>
          <img src="img/introduction/gioi_thieu_ve_luxury_life.jpg" style="width: 700px;">
          <p>Chuyên thiết kế và sản xuất trang phục thời trang công sở được thành lập tháng 5/2007 và đã nhanh chóng trở thành một công ty lớn trong ngành may mặc ở Việt Nam. Đặc biệt trong lĩnh vực thời trang công sở cao cấp. Đã phát triển thành hệ thống các cửa hàng bán lẻ phục vụ khách hàng. Liên kết với viện thời trang Italia nên mẫu mã luôn được cập nhật trực tiếp, phù hợp với nhu cầu và thị hiếu của thị trường. Sự kế hợp chặt chẽ và thống nhất cao của đội ngũ lãnh đạo công ty và các nhà thiết kế nước ngoài cùng với đội ngũ thợ may cẩn trọng, chau chuốt từng chi tiết, chúng tôi luôn mang đến quý khách hàng những sản phẩm chất lượng cao cấp nhất mang thương hiệu Luxury Life. Tất cả những gì chúng tôi mong muốn là một xã hội mà nơi đó người phụ nữ hiện đại được tôn vinh, thể hiện được chính mình. Phương châm làm việc của chúng tôi cống hiến tất cả vì sự hài lòng của khách hàng.</p>
          <p>Luxury Life - theo thần thoại Hy Lạp, có nghĩa là thiên đường sắc đẹp. Logo của Luxury Life muốn gửi gắm thông điệp về vẻ đẹp trường tồn cùng thời gian. Dù trong chúng ta ở vào địa vị và hoàn cảnh sống khác nhau - giàu sang hay đang bươn chải, thành đạt hay đang phải đương đầu với khó khăn, còn trẻ hay đã có tuổi - vẻ đẹp lấp lánh của mỗi con người là giá trị không thể phủ nhận và sẽ mãi mãi đi cùng chúng ta trong suốt cuộc đời. Luxury Life thực sự mong muốn được làm một cầu nối chuyển tải vẻ đẹp ấy"</p>
          <p>Luxury Life là nhãn hiệu thời trang cao cấp được đăng ký bảo hộ tại Việt nam. Thương hiệu Luxury Life có mặt tại thị trường Việt Nam từ tháng 5/2007 trụ sở chính của hãng tại  47 Đê Tô Hoàng, Hai Bà Trưng, Hà Nội trở thành một trong những thương hiệu thời trang công sở cao cấp hàng đầu tại Việt Nam.</p>
          <p>Các sản phẩm Luxury Life mang đậm phong cách Italia nhưng lại rất phù hợp với vóc dáng của người phụ nữ Việt Nam. Các sản phẩm là sự kết hợp tinh túy giữa thời trang Italia sang trọng và phong cách Á Đông thanh lịch. Đảm bảo sự sang trọng và quyến rũ mà không làm mất đi vẻ nữ tính của người phụ nữ Việt Nam.</p>
          <p>Thế mạnh lớn nhất của Luxury Life là các sản phẩm thời trang công sở dành cho các doanh nhân, các nữ nhân viên văn phòng, công chức và những người phụ nữ hiện đại thành đạt. Kiểu dáng và chất liệu cao cấp của Luxury Life sẽ làm cho quý vị cảm thấy trang phục công sở không còn là sự bắt buộc mà thực sự trở thành một niềm hứng khởi cho mỗi ngày mới bắt đầu. Mang đến cho chị em phụ nữ nét thanh lịch, sang trọng, duyên dáng và quyến rũ.</p>
          <p>Hiện nay trên thị trường Việt Nam, thương hiệu Luxury Life đã có một hệ thống các showroom chính thức tại Hà Nội và một số các cửa hàng lớn trên toàn quốc. Và không ngừng mở rộng thêm các showroom để phục vụ quý khách được tốt hơn. Tự hào với thành tích của chính mình, xong Luxury Life hiểu rằng tất cả hãy con ở phía trước. Không ngừng nghiên cứu, học hỏi và lắng nghe để dòng sản phẩm cao cấp mang thương hiệu Luxury Life ngày một đẳng cấp hơn. Đáp ứng tối đa nhu cầu của quý khách hàng và thị trường. Xin chân thành cảm ơn mọi sự đóng góp và ủng hộ của quý khách hàng đã dành cho Luxury Life trong suốt thời gian qua và trong tương lai.</p>

          

          <hr>

          </div>

      </div>
     
      

        </div>
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include('footer.php');?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
