<?php session_start();
if(isset($_SESSION['login_user']) && $_SESSION['login_user']==1)
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Luxury life | Danh sách trả lời comment</title>
    <link rel="shortcut icon" href="img/logo.jpg">

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/simple-sidebar.css" rel="stylesheet">
    <style type="text/css">
        th{text-align: center;}
    </style>
</head>
<body>
	<div id="wrapper">

        <!-- Sidebar -->
        <?php
        	include("simple-sidebar.php");
        ?>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">                       
                        <a href="#menu-toggle" class="btn btn-warning" id="menu-toggle" style="margin-bottom: 20px;  margin-left: 14px;">Menu >></a>
                        
                    </div>
                </div>
               <?php
                require ('../connect.php');
                $sql = "SELECT reply_comment.rep_comment_id as id, comment.comment_id as comment_id, product.name as product_name, username, comment.comment as comment, reply_comment.comment as rep_comment, reply_comment.date_added as date_added FROM comment join product on comment.product_id=product.product_id join customer on comment.customer_id=customer.customer_id join reply_comment on comment.comment_id=reply_comment.comment_id";
                $ketQuaTruyVan = $conn->query($sql);
                ?>
        <div class="container-fluid">
                <table class="table table-striped table-bordered table-hover table-condensed table-reponsive" style="text-align: center;">                    
                    <tr>
                        <th>Mã trả lời comment</th>
                        <th>Mã comment</th>                           
                        <th>Sản phẩm</th>
                        <th>Tài khoản khách hàng</th>
                        <th>Comment</th> 
                        <th>Nội dung trả lời</th>                         
                        <th>Ngày trả lời</th>
                        <th>Thao tác</th>
                    </tr>
                    <?php
                    if($ketQuaTruyVan->num_rows > 0){
                    while($rep_comment = $ketQuaTruyVan->fetch_assoc()){                    
                    ?>
                    <tr>                            
                        <td><?php echo $rep_comment['id']; ?></td>
                        <td><?php echo $rep_comment['comment_id']; ?></td>
                        <td><?php echo $rep_comment['product_name']; ?></td>
                        <td><?php echo $rep_comment['username']; ?></td>
                        <td><?php echo $rep_comment['comment']; ?></td>
                        <td><?php echo $rep_comment['rep_comment']; ?></td>

                        <td><?php echo date_format(new DateTime($rep_comment['date_added']),"d-m-Y H:i:s"); ?></td>
                        <td>
                            
                            <a class="btn btn-info" href="edit_rep_comment.php?id=<?php echo $rep_comment['id'];?>">Sửa</a>
                            <a class="btn btn-danger" href="del_rep_comment.php?id=<?php echo $rep_comment['id'];?>">Xóa</a>
                            
                        </td>
                    </tr>    
                    <?php  
                    }}
                    else{?>
                    <tr>
                        <td colspan="13" style="color: red">Không có bình luận nào</td>
                    </tr>
                    <?php } ?>
                </table>                    
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>
<?php    
}else{
    echo 
    "<script>
    alert('Bạn cần đăng nhập để quản trị');
    window.location = 'index.php';
    </script>";
}
?>