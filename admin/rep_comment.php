<?php session_start();
if(isset($_SESSION['login_user']) && $_SESSION['login_user']==1)
{
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Luxury life | Trả lời comment</title>
    <link rel="shortcut icon" href="img/logo.jpg">

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/simple-sidebar.css" rel="stylesheet">
    <!--JS-->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
   
    <script type="text/javascript">
      function validateForm()
      {
        var content = document.forms["form_rep_comment"]["comment"].value;
        if(content.trim()=="")
        {
            alert("Bạn chưa nhập nội dung ");
            document.forms["form_rep_comment"]["comment"].focus();
            return false;
        }
        
    
      }
    </script>
</head>
<body>
	<div id="wrapper">

        <!-- Sidebar -->
        <?php
        	include("simple-sidebar.php");
        ?>
        <!-- /#sidebar-wrapper -->
       <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">                       
                        <a href="#menu-toggle" class="btn btn-warning" id="menu-toggle" style="margin-bottom: 20px;  margin-left: 14px;">Menu >></a>
                    </div>
                </div>            
            <div class="container-fluid">
              <h1 align="center" style="padding-top: 10px">Trả lời comment</h1>
              <br>
              <br>
              
              <form class="form form-horizontal" method="post" action="replying_comment.php" id="form_rep_comment" onsubmit="return(validateForm());">
                
                <div class="form-group">
                    <label class="control-label col-sm-2">Nội dung</label>
                    <div class="col-sm-10">
                      <textarea name="comment" id="comment" rows="4" cols="120"></textarea>
                    </div>
                </div> 
                <input type="hidden" class="form-control" name="id" value="<?php echo $_GET['id']; ?>">
                  <div class="form-group">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                  <input class="btn btn-warning" type="submit" value="Lưu" onclick="saveButton()" />
                  </div>
                  </div>
              </form>                     
            </div>               
        </div>
    </div>


       </div>
    <!-- /#wrapper -->

   
    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>
<?php    
}else{
    echo 
    "<script>
    alert('Bạn cần đăng nhập để quản trị');
    window.location = 'index.php';
    </script>";
}
?>