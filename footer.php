<footer class="py-5" style="margin-top: 100px;">
    
      <div class="container" style="color: white;">
        <div class="row">
        <!-- about us -->
        <div class="col-lg-4 footer_01">
           <h4>VỀ CHÚNG TÔI</h4>            
            <ul>Chuyên thiết kế và sản xuất trang phục thời trang công sở được thành lập tháng 5/2007 và đã nhanh chóng trở thành một công ty lớn trong ngành may mặc ở Việt Nam, đặc biệt trong lĩnh vực thời trang công sở cao cấp.</ul>         
        </div>      
        <!-- connect -->  
        <div class="col-lg-4 footer_01">
          <h4>LUXURY LIFE</h4>                       
            <ul>47 Đê Tô Hoàng, Hai Bà Trưng, Hà Nội.</ul>
            <ul>HOTLINE: 2212 1309</ul>
            <ul>Email: longyeuhanh@luxurylife.com.vn</ul>
          <h4>SOCIAL CONNECTING</h4>
            <a href="https://www.facebook.com/"><img src="img/icon_footer/facebook.png" style="width: 30px; height: auto;"></a>
            <a href="https://www.instagram.com/"><img src="img/icon_footer/instagram.png" style="width: 45px; height: auto;"></a>         
          <h4>CUSTOMER CARE</h4>
            <p>SIZE GUIDE</p>
            <P>SHIPPING GUIDE</P>
        </div>
        <!-- Location -->
        <div class="col-lg-4 footer_01">
          <h4>ĐỊA CHỈ: Hà Nội</h4>
          <div class="map" style="margin-top: 20px;">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.62579399417!2d105.84823811450423!3d21.007632186009783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac757d872419%3A0x9ec3bf98fe2a99a9!2zNDcgxJHDqiBUw7QgSG_DoG5nLCBD4bqndSBE4buBbiwgSGFpIELDoCBUcsawbmcsIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1527680421231" width="310" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>  
        </div> 
        <hr>
        <div class="col-lg-12" style="border-top: 1px solid white;">
          <i>Copyright © 2018 Luxury Life</i>
        </div>
      </div>
      <!-- /.container -->
    </footer>