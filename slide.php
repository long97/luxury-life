<header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="height: 640px; background-color: #343a40;">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('img/slide/Special_Holiday.jpg');height:640px; ">
            <div class="carousel-caption d-none d-md-block">
              <h3>Special Holiday</h3>
              <p>Những sự kiện đặc biệt từ Luxury Life</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('img/slide/Fashion_Week.jpg');height:640px;">
            <div class="carousel-caption d-none d-md-block">
              <h3>Fashion Week</h3>
              <p>Bộ sưu tập ấn tượng ở tuần lễ thời trang Los Angeles</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('img/slide/New_Lookbook.jpg');height:640px;">
            <div class="carousel-caption d-none d-md-block">
              <h3>New lookbook</h3>
              <p>Đơn giản, nhẹ nhàng và tinh tế</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>